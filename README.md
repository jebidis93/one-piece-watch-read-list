| Episodes | Chapters | Filler? | Watch/Read | Other notes |
|----------|----------|---------|------------|-------------|
| `1-3` | `1-7` | NO | **READ** | The anime does things in a different order for some reason |
| `4-8` | `8-21` | NO | **READ** |  |
| `9-18` | `22-41` | NO | **READ** | Most people's least favorite arc.  But not skippable |
| `19-30` | `42-68` | NO | **EITHER** | Can either be **WATCH**ed or **READ**.  The anime changes something and makes that thing way less interesting |
| `31-44` | `69-95` | NO | **WATCH** | Good arc :+1: |
| `45-53` | `96-100` | Ep. `50-51` | **READ** | The filler for this arc is sort of canon? Apparently Oda wanted to include that stuff in the manga, but didn't. |
| `54-60` |  | YES | **SKIP** | bad filler |
| `61-63` | `101-105` | NO | **EITHER** |  |
| `64-67` | `106-114` | NO | **EITHER** |  |
| `68-69` |  | NO | ***Optional* WATCH** | These are adaptations of the cover stories.  So these are canon, but may be skippable if you **READ** the chapters that had this cover story. |
| `70-77` | `115-129` | NO | **EITHER** | Can do either, doesn't matter which |
| `78-91` | `130-154` | NO | **EITHER** | There are some cool scenes that make me lean toward **WATCH**, but it doesn't matter which you do |
| `92-130` | `155-217` | Ep. `93`, `98`, `99`, `101`, `102` | **WATCH** | Skip the filler episodes |
| `131-143` |  | YES | **SKIP** | bad filler |
| `144-152` | `218-236` | NO | **READ+WATCH** | **READ** Ch. `218-231`, then **WATCH** Ep. `150-152` |
| `153-195` | `237-302` | NO | **EITHER** | Lean towards **READ** |
| `196-206` |  | YES | ***Optional* WATCH** | This is literally the only filler arc I've ever thought was good in any show |
| `207-228` | `303-321` | Ep. `213-216` | **READ** | This is like the worst arc in the anime.  Most people that watch it think it is filler, but only most of it is filler, so definitely **READ** |
| `229-325` | `322-441` | Ep. `279-283`, `291-292`, `303`, `317-319` | **WATCH** | This is technically 3 different arcs, but I consider them all as one arc.  This is my favorite arc by far. **SKIP** the filler episodes. |
| `326-336` |  | YES | **SKIP** | bad filler |
| `337-381` | `442-489` | NO | **READ+WATCH** | **READ** Ch.`442-475`, **WATCH** Ep. `370-381` |
| `382-384` |  | YES | **SKIP** | bad filler |
| `385-405` | `490-513` | NO | **READ** |  |
| `406-407` |  | YES | **SKIP** | bad filler |
| `408-421` | `514-524` | NO | **READ** |  |
| `422-452` | `525-549` | Ep. `426-429` | **EITHER** | You may want to **WATCH** the filler Ep. for more context for the movie `Strong World` |
| `Strong World` |  | MOVIE | ***Optional* WATCH** | Kind of a based movie.  Throws off the powerscaling of the story WILDLY though since it is a semi-canon movie |
| `Strong World: Episode 0` | `Strong World: Chapter 0` | MAYBE? | ***Optional* WATCH** | The chapter was written by Oda so it might be canon? It's kinda cool though so I wouldn't skip |
| `453-456` |  | SORT OF | ***Optional* WATCH** | This is kinda filler, but kinda not.  I think this is covered in the cover stories of certain chapters, but I am not sure. I would probably watch. |
| `457-489` | `550-580` | NO | **EITHER** | I have yet to read this one, but from what I know, the manga is way better.  This is most people's favorite arc, even the anime only's so you can either **WATCH** or **READ**, but I would lean towards **READ** |
| `490-516` | `581-597` | Ep. **`492`**, `498-499` | **READ** | Skip filler, but see below table about the Ep. `492` filler |
| `517-522` | `598-602` | NO | **READ** |  |
| `523-574` | `603-653` | Ep. **`542`** | **READ** | Boring ass arc, but important for the story so definitely do not skip.  See below table about the Ep. `542` filler |
| `575-578` |  | YES | ***Optional* WATCH** | May want to watch for more context for `Film: Z` |
| `Film: Z` |  | MOVIE | ***Optional* WATCH** | Cool movie |
| `579-625` | `654-699` | Ep. **`590`** | **READ** | You could do **EITHER**, but I would lean towards **READ**.  See below table about the Ep. `590` filler |
| `626-628` |  | YES | **SKIP** | bad filler |
| `629-746` | `700-801` |  | **READ** | Do not **WATCH** this arc, they messed it up so badly.  Definitely **READ** |
| `747-750` |  | YES | ***Optional* WATCH** | Watch this if you want more context for `Film: Gold` |
| `Heart of Gold` |  | YES | ***Optional* WATCH** | Watch this if you want more context for `Film: Gold` |
| `Film: Gold` |  | MOVIE | ***Optional* WATCH** | This movie is OK.  One character has an insanely broken power, and the final fight is pretty cool. |
| `751-779` | `802-824` | NO | **EITHER** | Based arc, **WATCH** or **READ**, doesn't matter |
| `780-782` |  | YES | **SKIP** | all right filler |
| `783-877` | `825-902` | NO | **HARD TO SAY** | This is one of my favorite arcs.  I **READ** and **WATCH**ed it. At times, the anime drags on a bit, but throughout the arc there are parts that I think are worth **WATCH**ing.  I may revise this part and give more exact chapters/episodes. |
| `878-889` | `903-908` | NO | **READ** | The anime _RUINED_ what should have been one of the best arcs.  Absolutely **READ**, do not **WATCH** |
| `890-1085` | `909-1057` | Ep. `895-896`, `907`, `1029-1030` | **EITHER** | This is by far the longest arc.  This is when the modern artstyle really comes out in the anime so you may want to **WATCH** this arc.  But also its so long you may just want to **READ**.  The filler for this arc is interesting.  See more about that below. |
| `Stampede` |  | MOVIE | **WATCH** | Best movie.  Don't even remember if there was a story.  It was literally just fighting for an hour and a half.  If you are going to **WATCH** this, you may want to watch Ep. `895-896` filler episodes for more context |
| `Film: Red` |  | MOVIE | ***Optional* WATCH** | Its an ok movie.  Its a musical which is lame, but the main villain is cool.  Its kinda weird, because its not canon but Oda seems like he is trying to retcon some things with this movie.  Maybe **WATCH**. If you are going to **WATCH** this, you may also want to **WATCH** the filler Ep. `1029-1030` for more context |
| `1086-???` | `1058-???` | NO | **DON'T KNOW YET** | This is the current arc.  I **READ** and **WATCH** this weekly.  Its that good of an arc |

# Filler info

## Ep. 492, 542, 590
- These are crossovers with [Toriko](https://toriko.fandom.com/wiki/Toriko_(Anime)) and [Dragon Ball Z](https://dragonball.fandom.com/wiki/Dragon_Ball_Z).  So if you like those other shows, maybe you want to watch these.

## Ep. 907
- This is the alternate version to the start of One Piece.  From way back when Oda wasn't sure how he wanted to start the story.  Not bad to watch.